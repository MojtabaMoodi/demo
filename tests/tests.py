import os

from django.contrib.auth.password_validation import validate_password
from django.test import TestCase


class TryDjangoConfigTest(TestCase):
    def test_secret_key_strength(self):
        secret_key = os.environ.get("DJANGO_SECRET_KEY")
        try:
            validate_password(secret_key)
        except Exception as e:
            msg = f"Weak Secret Key. {e.messages}"
            self.fail(msg)
