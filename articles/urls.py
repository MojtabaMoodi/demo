from django.urls import path

from . import views

app_name = "articles"

urlpatterns = [
    path("", views.article_search_view),
    path("create/", views.article_create_view, name="article-create"),
    path("<slug:slug>/", views.article_detail_view, name="article-detail"),
]
